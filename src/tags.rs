//! # Tags:
//!
//! Provides more easy to use functions with the known HTML tags.
//! For example, instead of `Element::new("div").children(["hello, world"])`, `div(["hello, world"], [])` could be used instead.
//!
//! ## Usage:
//! This module provides a function to every HTML tag as of 12/2023.
//! Because of that, this module follows the pattern: `{tag}({attributes}[], {children}[])`
//!
//! ## Example:
//! ```
//! use shele::prelude::*;
//!
//! let elements_module = Element::new("div").add_child("hello, world");
//! let tags_module = div([], ["hello_world"]);
//!
//! assert_eq!(elements_module, tags_module);
//! ```
//!
//! ## Known Limitations:
//! - Children cannot be different types: when using this module, the compiler tries to guess the type of the Children based on the first element.
//! Because of that, the compiler will only accept items in the array that share the same type as the first child.

use std::collections::HashMap;

use crate::element::{template::Template, Element};

type Attributes<'a, const N: usize> = [(&'a str, &'a str); N];

fn el<'a, T, const NCONTENT: usize, const NATTR: usize>(
    tag_name: impl ToString,
    attrs: Attributes<'a, NATTR>,
    content: [T; NCONTENT],
) -> Element<'a>
where
    T: Template + 'a,
{
    let attrs = HashMap::from_iter(attrs);

    Element::new(tag_name).attributes(attrs).children(content)
}

macro_rules! generate_tag {
    [$($x:ident),*] => {
        $(
            pub fn $x<'a, T, const NCONTENT: usize, const NATTR: usize>(
                attrs: Attributes<'a, NATTR>,
                content: [T; NCONTENT],
            ) -> Element<'a>
            where
                T: Template + 'a,
            {
                el(stringify!($x), attrs,content)
            }
        )*
    };
}

generate_tag![
    a, abbr, address, area, article, aside, audio, b, base, bdi, bdo, blockquote, body, br, button,
    canvas, caption, cite, code, col, colgroup, data, datalist, dd, del, details, dfn, dialog, div,
    dl, dt, em, embed, fieldset, figcaption, figure, footer, form, h1, h2, h3, h4, h5, h6, head,
    header, hr, html, i, iframe, img, input, ins, kbd, label, legend, li, link, main, map, mark,
    meta, meter, nav, noscript, object, ol, optgroup, option, output, p, param, picture, pre,
    progress, q, rp, rt, ruby, s, samp, script, section, select, small, source, span, strong,
    style, sub, summary, sup, svg, table, tbody, td, template, textarea, tfoot, th, thead, time,
    title, tr, track, u, ul, var, video, wbr
];
