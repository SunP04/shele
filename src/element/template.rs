use std::fmt;

/// Internal representation for anything that can be inside of an HTML Element.
/// * May be changed in the future.
pub trait Template: ToString + fmt::Debug {}

impl<T: ToString + fmt::Debug> Template for T {}
