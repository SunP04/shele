pub(crate) mod template;

use std::collections::HashMap;
use std::{fmt, hash};

use template::Template;

/// Represents a children in a HTML Element.
///
/// Used as a Box for usage of any type that implements the `Template` trait.
pub type Children<'a> = Vec<Box<dyn Template + 'a>>;

/// Represents the attributes in a HTML Element.
///
/// Attributes are all the parameters that are not tags.
/// For example, using the `styles` attribute could be used to change the css in a given Element.
pub type Attributes<'a> = HashMap<&'a str, &'a str>;

/// Struct representing a HTML Element.
/// Any content that would be inside a tag, is considered a Child.
/// Any parameter or prop to the element, is considered an Attribute.
///
/// ```
/// use shele::element::Element;
///
/// // Creating a div with a red text
/// let el = Element::new("div")
///    .add_child("Hello, world")
///    .add_attr("styles", "color: red;");
///
/// assert_eq!(el.to_string(), "<div styles=\"color: red;\">Hello, world</div>");
/// ```
#[derive(Debug)]
pub struct Element<'a> {
    tagname: String,
    children: Children<'a>,
    attrs: Attributes<'a>,
}

impl<'a> Element<'a> {
    /// Creates a new HTML Element
    pub fn new(tagname: impl ToString) -> Self {
        Self {
            tagname: tagname.to_string(),
            children: Vec::new(),
            attrs: HashMap::new(),
        }
    }

    /// Creates a new HTML Element with prepopulated children
    ///
    /// Same as calling `Element::new`, followed by a `Element::children`
    pub fn with<T: Template + 'a, const N: usize>(tagname: impl ToString, content: [T; N]) -> Self {
        Self::new(tagname).children(content)
    }

    /// Getter for the tag of the HTML Element.
    pub fn get_tag_name(&self) -> &str {
        self.tagname.as_str()
    }

    /// Getter for the internal representation of children for an HTML Element.
    /// The returning type is the internal representation of the children.
    /// Since the type is boxed, there will be no difference in the API.
    pub fn get_children(&self) -> &Children {
        &self.children
    }

    /// Getter for the attributes of an HTML Element.
    pub fn get_attrs(&self) -> &Attributes {
        &self.attrs
    }

    /// Adds one child to the end of the children.
    pub fn add_child<T: Template + 'a>(mut self, template: T) -> Self {
        self.children.push(Box::new(template));

        self
    }

    /// Adds one attribute to Element.
    /// If an attribute with the same key already exists, it will be replaced by the new value.
    pub fn add_attr(mut self, key: &'a str, value: &'a str) -> Self {
        self.attrs.insert(key, value);

        self
    }

    /// Replaces all the content(children) inside this Element.
    /// Unlike the `Element::children` function, this function uses the internal representation of a children, without doing any checks.
    pub fn children_boxed(mut self, ch: Children<'a>) -> Self {
        self.children = ch;

        self
    }

    /// Replaces all the content(children) inside this Element.
    pub fn children<T: Template + 'a, const N: usize>(mut self, ch: [T; N]) -> Self {
        self.children = ch
            .into_iter()
            .map(|c| -> Box<dyn Template> { Box::new(c) })
            .collect::<Children<'a>>();

        self
    }

    /// Replaces all the attributes inside this Element.
    pub fn attributes(mut self, attrs: Attributes<'a>) -> Self {
        self.attrs = attrs;

        self
    }
}

impl<'a> fmt::Display for Element<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let children = self
            .get_children()
            .iter()
            .map(|templ| templ.to_string())
            .collect::<String>();

        let attrs = self
            .get_attrs()
            .iter()
            .map(|(k, v)| format!("{k}=\"{v}\""))
            .collect::<Vec<_>>()
            .join(" ");

        let tag = format!(
            "<{tag} {attrs}>{child}</{tag}>",
            tag = self.get_tag_name(),
            child = children,
            attrs = attrs,
        )
        .replace(" >", ">");

        write!(f, "{}", tag)
    }
}

impl<'a> Eq for Element<'a> {}
impl<'a> PartialEq for Element<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.tagname == other.get_tag_name() && self.attrs == *other.get_attrs()
    }
}

impl<'a> Ord for Element<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.to_string().cmp(&other.to_string())
    }
}

impl<'a> PartialOrd for Element<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.to_string().partial_cmp(&other.to_string())
    }
}

impl<'a> Default for Element<'a> {
    fn default() -> Self {
        Self::new("")
    }
}

impl<'a> hash::Hash for Element<'a> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        let stringified = self.to_string();
        let bytes = stringified.as_bytes();

        state.write(bytes);
        state.finish();
    }
}
