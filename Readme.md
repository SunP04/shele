# Shele
#### Sun's HTML Elements
A minimalist HTML Builder for Rust.

Provides the basic building blocks for creating HTML Elements.

#### Attention: This crate is very simple and may not be a good fit for bigger projects.

## Modules:
- [Element](https://docs.rs/shele/latest/shele/elements/index.html) - Provides the basic Element builder API. Used internally to create other functions.
- [Tags](https://docs.rs/shele/latest/shele/tags/index.html) - Provides an API that is easier to use.

## Links:
<ul>
 <li>
   <a href="https://gitlab.com/SunP04" target="_blank">Author</a>
 </li>
 <li>
   <a href="https://gitlab.com/SunP04/shele" target="_blank">Project Repository</a>
 </li>
</ul>

Any complain or bug/issue are welcome!
If you want to contribute, just read to the [shele](https://gitlab.com/SunP04/shele) repository.
